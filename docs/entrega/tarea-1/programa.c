#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

int main(int argc, char* argv[], char* envp[])
{
  /* Inicializando buffer */
  size_t tamanio=64;
  char mensaje[tamanio];
  bzero(mensaje, tamanio);

  /* Inicializa informacion del equipo */
  char TEAM[] = "Equipo-MMMA-MMP-TBM";
  char MARTINEZ[] = "1234567890\tMiguel Ángel Martínez Mendoza";
  char MONTOYA[] = "312195362\tPedro Montoya Montes";
  char TORRES[] = "1234567890\tMiriam Torres Bucio";

  /* Si no se dan parametros se lanza el mensaje con salida -1 */
  if (argc == 1)
  {
    fprintf(stderr,"%s: Could not parse arguments\n", argv[0]);
    fprintf(stderr,"Usage:\n\t%s <integer state> [optional text]\n", argv[0]);
    return -1;
  } 
  /* Obtiene parametros para procesar */
  else
  {
    /* Parametro de version */
    if (strcmp(argv[1], "-V") == 0 || strcmp(argv[1], "--version")==0)
    {
        fprintf(stderr,"%s v0.0.1 (Sistemas Operatrivos - %s)\n", argv[0], TEAM);
        fprintf(stderr,"%s\n%s\n%s\n", MARTINEZ, MONTOYA, TORRES);
        return -2;  
    }
    /* Parametro de ayuda */
    else if (strcmp(argv[1], "-h" ) == 0 || strcmp(argv[1], "--help")==0)
    {
      fprintf(stderr,"%s v0.0.1 (Sistemas Operatrivos - %s)\n", argv[0], TEAM);
      fprintf(stderr,"%s\n%s\n%s\n", MARTINEZ, MONTOYA, TORRES);
      fprintf(stderr,"\nThis plugin will simply return the state corresponding to the numeric value of the <state> argument with optional text");
      fprintf(stderr,"\n\nUsage:\n\t <integer state> [optional text]\n");
      fprintf(stderr,"\nOptions:\n\t-h, --help\n\t\tPrint detailed help screen\n\t-V, --version\n\t\tPrint version information\n");
      return -3;
    }

    else if (strcmp(argv[1],"0") == 0)
    {
      fprintf(stderr,"OK\n");
      return 0;
    }
    else if (strcmp(argv[1],"1") == 0)
    {
      fprintf(stderr,"WARNING\n");
      return 1;
    }
    else if (strcmp(argv[1],"2") == 0)
    {
      fprintf(stderr,"CRITICAL\n");
      return 2;
    }
    else if (strcmp(argv[1],"3") == 0)
    {
      fprintf(stderr,"UNKNOWN\n");
      return 3;
    }
    else
    {
      fprintf(stderr,"FAILURE: Status %s is not supported\n",argv[1]);
      return -4;
    }
  }

  /* Código de salida */
  return EXIT_SUCCESS;
}
